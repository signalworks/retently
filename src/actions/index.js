import {
  CHANGE_TEXT,
  RECV_ITEMS,
  CHANGE_OFFSET,
  ADD_CHECKED,
  REMOVE_CHECKED,
  CLEAR_CHECKED,
  CHANGE_SEARCH
} from '../constants'


export function changeText(text) {
  return {
    type: CHANGE_TEXT,
    payload: text
  };
}

export function changeOffset(offset) {
  return {
    type: CHANGE_OFFSET,
    payload: offset
  };
}

export function addChecked(id) {
  return {
    type: ADD_CHECKED,
    payload: id
  };
}

export function removeChecked(id) {
  return {
    type: REMOVE_CHECKED,
    payload: id
  };
}

export function clearChecked() {
  return {
    type: CLEAR_CHECKED
  };
}

export function  changeSearch(value) {
  return {
    type: CHANGE_SEARCH,
    payload: value
  };
}

export function requestItems(params) {
  let str = Object.keys(params).map(
    (key) => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`
  ).join('&');

  var xhr = new XMLHttpRequest();
  xhr.open('GET', `https://pure-island-2586.herokuapp.com/v1/customers?${str}`, false);
  xhr.send();
  var json = JSON.parse(xhr.responseText);

  return {
    type: RECV_ITEMS,
    payload: json
  };
}
