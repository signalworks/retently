import React, { Component } from 'react'
import { connect } from 'react-redux'
import { changeText, requestItems, changeOffset, addChecked,
         removeChecked, clearChecked, changeSearch } from '../actions'
import Row from '../components/Row'

class App extends Component {
  componentWillMount() {
    this.props.dispatch(requestItems({ offset: 0 }));
  }

  componentWillReceiveProps(props, state) {
    const {dispatch, offset, isSearch } = this.props;
    if (props.offset != offset || props.isSearch != isSearch) {
      var params = { offset: props.offset };
      if (props.isSearch) {
        params.q = props.input_text;
      }
      dispatch(requestItems(params));
    }
  }

  changeText(e) {
    this.props.dispatch(changeText(e.target.value));
  }

  next(e) {
    const { offset, total, dispatch } = this.props;
    const _offset = (offset < total - 1) ? (offset + 10) : (total - 1);
    if (_offset != offset) {
      dispatch(clearChecked());
      dispatch(changeOffset(_offset));
    }
  }

  back(e) {
    const { offset, dispatch } = this.props;
    const _offset = (offset > 0) ? (offset - 10) : 0;
    if (_offset != offset) {
      dispatch(clearChecked());
      dispatch(changeOffset(_offset));
    }
  }

  search(e) {
    const { input_text, dispatch } = this.props;
    dispatch(clearChecked());
    dispatch(changeOffset(0));
    dispatch(changeSearch(Boolean(input_text)));
  }

  render() {
    const {
      offset, total, customers, input_text, checked, dispatch
    } = this.props;

    if (!customers) {
      return <div></div>
    }

    return <center>
      <input type='text' placeholder='search' value={ input_text } onChange={ this.changeText.bind(this) }/>
      <button onClick={ this.search.bind(this) }> search </button>
      <br/> <br/>
      <table width='80%' id='table'>
        <thead>
        <tr>
          <th> check </th>
          <th> first name </th>
          <th> last name </th>
          <th> e-mail </th>
          <th> company </th>
          <th> created date </th>
          <th> action </th>
        </tr>
        </thead>
        <tbody>
        {customers.map(({ _id, firstName, lastName, email, companyName, createdDate }) => {
          return <Row
            dispatch={ dispatch }
            key={ _id }
            _id={ _id }
            firstName={ firstName }
            lastName={ lastName }
            email={ email }
            companyName={ companyName }
            createdDate={ createdDate }
            checked={ checked.indexOf(_id) != -1 }
          />
        })}
        </tbody>
      </table>
      <br/>
      <button onClick={ (e) =>  (checked.length ? alert(checked.join('\n')) : '') }> show </button>
      <button className='right' onClick={ (e) => alert(customers.map((c) => c._id).join('\n')) }> show all </button>
      <button className='left' onClick={ this.back.bind(this) }> &lt; </button>
      <button className='right' onClick={ this.next.bind(this) }> &gt; </button>
      <span className='right'>
        page: { Math.ceil(offset / 10 + 1) } / { Math.ceil(total / 10) }
      </span>
    </center>
  }
}

function mapStateToProps(state) {
  return {
    customers: state.customers,
    offset: state.offset,
    total: state.total,
    input_text: state.input_text,
    checked: state.checked,
    isSearch: state.isSearch
  }
}

export default connect(mapStateToProps)(App)