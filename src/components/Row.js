import React, { PropTypes, Component } from 'react'
import { addChecked, removeChecked } from '../actions'

export default class Row extends Component {
  constructor() {
    super();
    this.state = { checked: false };
  }

  componentWillReceiveProps(props, state) {
    this.setState({ checked: props.checked });
  }

  shouldComponentUpdate(props, state) {
    if (state.checked != this.state.checked) {
      return true;
    }

    return false;
  }

  check(e) {
    const { _id, dispatch } = this.props;
    if (e.target.checked) {
      dispatch(addChecked(_id));
    } else {
      dispatch(removeChecked(_id));
    }
    this.setState({checked: e.target.checked});
  }
  
  render() {
    const { _id, firstName, lastName, email, companyName, createdDate } = this.props;
    const date = createdDate.split('T').join(' ').slice(0, createdDate.length - 5);
    return <tr>
      <td>
        <input type='checkbox' checked={ this.state.checked } onChange={ this.check.bind(this) }/>
      </td>
      <td> { firstName } </td>
      <td> { lastName } </td>
      <td> { email } </td>
      <td> { companyName } </td>
      <td> { date }  </td>
      <td>
        <center>
          <input type='submit' value='show id' onClick={ (e) => alert(_id) }/>
        </center>
      </td>
    </tr>
  }
}

Row.propTypes = {
  dispatch: PropTypes.func.isRequired,
  _id: PropTypes.string.isRequired,
  firstName: PropTypes.string.isRequired,
  lastName: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  companyName: PropTypes.string.isRequired,
  createdDate: PropTypes.string.isRequired,
  checked: PropTypes.bool.isRequired
};