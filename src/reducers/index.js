import {
  RECV_ITEMS,
  CHANGE_TEXT,
  CHANGE_OFFSET,
  ADD_CHECKED,
  REMOVE_CHECKED,
  CLEAR_CHECKED,
  CHANGE_SEARCH
} from '../constants'

const initialState = {
  customers: [],
  input_text: '',
  offset: 0,
  total: 0,
  checked: [],
  isSearch: false
};

export default function reducer(state = initialState, action) {
  switch (action.type) {

    case CHANGE_TEXT:
      return { ...state, input_text: action.payload };

    case RECV_ITEMS:
      return { ...state, customers: action.payload['results'], total: action.payload['total'] };

    case CHANGE_OFFSET:
      return { ...state, offset: action.payload };

    case ADD_CHECKED:{
      let checked = state.checked;
      if (checked.indexOf(action.payload) == -1) {
        checked.push(action.payload);
      }
      return { ...state, checked: checked };
    }

    case REMOVE_CHECKED:{
      let checked = state.checked;
      const index = checked.indexOf(action.payload);
      if (index != -1) {
        checked.splice(index);
      }
      return { ...state, checked: checked };
    }
          
    case CLEAR_CHECKED:
      return { ...state, checked: [] };
    
    case CHANGE_SEARCH:
      return { ...state, isSearch: action.payload };
    
    default:
      return state;
  }
}
